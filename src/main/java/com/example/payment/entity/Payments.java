package com.example.payment.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "payments")
public class Payments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Column(name = "payment_id")
    Long id;
    @Column(name = "payment_amount")
    double amount;
    @Column(name = "check_date")
    LocalDate checkData;
    @Column(name = "course_month")
    String courseMonth;

    @ManyToOne
    @JoinColumn(name = "course_id")
    Course course;

    @ManyToOne
    @JoinColumn(name = "student_id")
    Student studentPhone;
    @Column(name = "check_type")
    String checkType;
    @Column(name = "card_holder")
    String cardHolder;


}
