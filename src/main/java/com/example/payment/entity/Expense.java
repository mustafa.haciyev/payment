package com.example.payment.entity;

import com.example.payment.repository.CourseRepository;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "expenses")
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Column(name = "expense_id")
    Long id;
    @Column(name = "expense_amount")
    double amount;
    @Column(name = "expense_date")
    LocalDate date;
    @Column(name = "expense_name")
    String expenseName;
    @Column(name = "expense_description")
    String description;
    @Column(name = "image_url")
    String imageUrl;

    @ManyToOne
    @JoinColumn(name = "student_id")
    Student student;

    @ManyToOne
    @JoinColumn(name = "course_id")
    Course course;

}
