package com.example.payment.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.Builder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    Long id;
    @Column(name = "student_mail")
    String studentMail;
    @Column(name = "student_phone")
    String studentPhone;
    @Column(name = "student_name")
    String studentName;
    @Column(name = "student_surname")
    String studentSurname;

    @ManyToOne
    @JoinColumn(name = "course_id")
    Course course;


}
