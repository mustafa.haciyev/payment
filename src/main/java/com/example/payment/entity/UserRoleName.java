package com.example.payment.entity;

public enum UserRoleName {
    ROLE_USER,
    ROLE_ADMIN
}
