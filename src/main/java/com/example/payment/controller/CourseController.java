package com.example.payment.controller;

import com.example.payment.entity.Course;
import com.example.payment.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping
    public List<Course> getAllCourse(){
        return courseService.getAllCourse();
    }

    @GetMapping("/{id}")
    public Course getCourseById(@PathVariable Long id){
        return courseService.getCourseById(id);
    }

    @GetMapping("/searchcourse")
    public List<Course> searchCourseByCourseName(@RequestParam String courseName){
        return searchCourseByCourseName(courseName);
    }

    @PostMapping
    public Course saveCourse(@RequestBody Course course){
        return courseService.saveCourse(course);
    }
    @PutMapping("/{id}")
    public ResponseEntity<Course> updateCourse(@PathVariable Long id, @RequestBody Course updatedCourse) {
        Course updatedCourseResult = courseService.updateCourse(id, updatedCourse);
        return updatedCourseResult != null ? ResponseEntity.ok(updatedCourseResult) : ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}")
    public void deleteByCourseId(@PathVariable Long id){
        courseService.deleteCourseId(id);
    }
}
