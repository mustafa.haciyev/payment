package com.example.payment.controller;

import com.example.payment.entity.Expense;
import com.example.payment.service.ExpenseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/expenses")
@RequiredArgsConstructor
public class ExpenseController {


    private final ExpenseService expenseService;

    @GetMapping
    public List<Expense> getAllExpenses(){
        return expenseService.getAllExpenses();
    }

    @GetMapping("/{id}")
    public Expense getByExpenseId(@PathVariable Long id){
        return expenseService.getByExpenseId(id);
    }

    @PostMapping
    public Expense saveExpense(@RequestBody Expense expense){
        return expenseService.saveExpense(expense);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Expense> updateExpense(@PathVariable Long id, @RequestBody Expense updatedExpense) {
        Expense updatedExpenseResult = expenseService.updateExpense(id, updatedExpense);
        return updatedExpenseResult != null ? ResponseEntity.ok(updatedExpenseResult) : ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}")
    public void deleteByExpenseId(@PathVariable Long id){
        expenseService.deleteByExpenseId(id);
    }


}
