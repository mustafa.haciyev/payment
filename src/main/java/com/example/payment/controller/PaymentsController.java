package com.example.payment.controller;

import com.example.payment.entity.Payments;
import com.example.payment.entity.Student;
import com.example.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PaymentsController {

    private final PaymentService paymentService;

    @GetMapping
    public List<Payments> getAllPayments(){
        return paymentService.getAllPayments();
    }
    @GetMapping("/{id}")
    public Payments getPaymentId(@PathVariable Long id){
        return paymentService.getPaymentById(id);
    }

    @GetMapping("/search")
    public List<Payments> searchPaymentsByStudentPhone(@RequestParam Student studentPhone){
        return paymentService.searchPaymentsByStudentPhone(studentPhone);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Payments> updatePayment(@PathVariable Long id, @RequestBody Payments updatedPayment) {
        Payments updatedPaymentResult = paymentService.updatePayment(id, updatedPayment);
        return updatedPaymentResult != null ? ResponseEntity.ok(updatedPaymentResult) : ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{id}")
    public void deletePayments(@PathVariable Long id){
        paymentService.deletePayment(id);
    }




}
