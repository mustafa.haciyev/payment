package com.example.payment.repository;

import com.example.payment.entity.Payments;
import com.example.payment.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payments, Long> {
    List<Payments> findByStudentPhoneContaining(Student studentPhone);
}
