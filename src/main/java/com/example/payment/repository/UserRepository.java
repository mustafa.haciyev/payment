package com.example.payment.repository;

import com.example.payment.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    // Ekstra metotlar burada tanımlanabilir
}
