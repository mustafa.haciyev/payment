package com.example.payment.service;

import com.example.payment.entity.Course;

import java.util.List;

public interface CourseService {
    List<Course> getAllCourse();

    Course getCourseById(Long id);
    List<Course> findByCourseNameContaining (String courseName);

    Course saveCourse(Course course);
    Course updateCourse(Long id, Course updatedCourse);

    void deleteCourseId(Long id);
}
