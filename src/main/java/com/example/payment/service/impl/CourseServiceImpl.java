package com.example.payment.service.impl;

import com.example.payment.entity.Course;
import com.example.payment.repository.CourseRepository;
import com.example.payment.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> getAllCourse() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourseById(Long id) {
        return courseRepository.findById(id).orElse(null);
    }

    @Override
    public List<Course> findByCourseNameContaining(String courseName) {
        return courseRepository.findByCourseNameContaining(courseName);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }
    @Override
    public Course updateCourse(Long id, Course updatedCourse) {
        Course existingCourse = courseRepository.findById(id).orElse(null);

        if (existingCourse != null) {
            existingCourse.setCourseName(updatedCourse.getCourseName());
            return courseRepository.save(existingCourse);
        }

        return null;
    }
    @Override
    public void deleteCourseId(Long id) {
        courseRepository.deleteById(id);
    }
}
