package com.example.payment.service.impl;

import com.example.payment.entity.Student;
import com.example.payment.repository.CourseRepository;
import com.example.payment.repository.StudentRepository;
import com.example.payment.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudentById(Long id) {
        return studentRepository.findById(id).orElse(null);
    }


    @Override
    public List<Student> searhStudentByStudentPhone(String studentPhone) {
        return studentRepository.findByStudentPhoneContaining(studentPhone);
    }

    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }
    @Override
    public Student updateStudent(Long id, Student updatedStudent) {
        Student existingStudent = studentRepository.findById(id).orElse(null);

        if (existingStudent != null) {
            BeanUtils.copyProperties(updatedStudent, existingStudent, "id");
            return studentRepository.save(existingStudent);
        }

        return null;
    }
    @Override
    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }


}
