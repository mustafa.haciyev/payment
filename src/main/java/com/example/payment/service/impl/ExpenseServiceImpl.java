package com.example.payment.service.impl;

import com.example.payment.entity.Expense;
import com.example.payment.repository.ExpenseRepository;
import com.example.payment.service.ExpenseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseServiceImpl implements ExpenseService {
    @Autowired
    private ExpenseRepository expenseRepository;
    @Override
    public List<Expense> getAllExpenses() {
        return expenseRepository.findAll() ;
    }

    @Override
    public Expense getByExpenseId(Long id) {
        return expenseRepository.findById(id).orElse(null);
    }

    @Override
    public Expense saveExpense(Expense expense) {
        return expenseRepository.save(expense);
    }

    @Override
    public Expense updateExpense(Long id, Expense updatedExpense) {
        Expense existingExpense = expenseRepository.findById(id).orElse(null);

        if (existingExpense != null) {
            BeanUtils.copyProperties(updatedExpense, existingExpense, "id");
            return expenseRepository.save(existingExpense);
        }

        return null;
    }
    @Override
    public void deleteByExpenseId(Long id) {
        expenseRepository.deleteById(id);
    }
}
