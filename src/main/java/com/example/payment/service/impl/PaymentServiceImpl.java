package com.example.payment.service.impl;

import com.example.payment.entity.Payments;
import com.example.payment.entity.Student;
import com.example.payment.repository.PaymentRepository;
import com.example.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Override
    public List<Payments> getAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public Payments getPaymentById(Long id) {
        return paymentRepository.findById(id).orElse(null);
    }

    @Override
    public List<Payments> searchPaymentsByStudentPhone(Student studentPhone) {
        return paymentRepository.findByStudentPhoneContaining(studentPhone);
    }

    @Override
    public Payments savePayment(Payments payments) {
        return paymentRepository.save(payments);
    }
    @Override
    public Payments updatePayment(Long id, Payments updatedPayment) {
        Payments existingPayment = paymentRepository.findById(id).orElse(null);

        if (existingPayment != null) {
            BeanUtils.copyProperties(updatedPayment, existingPayment, "id");
            return paymentRepository.save(existingPayment);
        }

        return null;
    }
    @Override
    public void deletePayment(Long id) {
        paymentRepository.deleteById(id);
    }
}
