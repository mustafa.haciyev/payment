package com.example.payment.service;

import com.example.payment.entity.Payments;
import com.example.payment.entity.Student;

import java.util.List;

public interface PaymentService {
    List<Payments> getAllPayments();
    Payments getPaymentById(Long id);
    List<Payments> searchPaymentsByStudentPhone(Student studentPhone);
    Payments savePayment(Payments payments);
    void deletePayment(Long id);
    Payments updatePayment(Long id, Payments updatedPayment);



}
