package com.example.payment.service;

import com.example.payment.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();

    Student getStudentById(Long id);

    List<Student> searhStudentByStudentPhone(String studentPhone);

    Student saveStudent(Student student);

    void deleteById(Long id);

    Student updateStudent(Long id, Student updatedStudent);


}
