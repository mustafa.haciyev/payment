package com.example.payment.service;

import com.example.payment.entity.Expense;

import java.util.List;

public interface ExpenseService {
    List<Expense> getAllExpenses();

    Expense getByExpenseId(Long id);

    Expense saveExpense(Expense expense);
    Expense updateExpense(Long id, Expense updatedExpense);

    void deleteByExpenseId(Long id);
}
